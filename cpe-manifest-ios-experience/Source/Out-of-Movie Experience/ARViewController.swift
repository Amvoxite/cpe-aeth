//
//  ARViewController.swift
//

import UIKit
import CPEData
import ARKit

@available(iOS 11.0, *)
class ARViewController: UIViewController, ARSCNViewDelegate {
    
    private var sceneView: ARSCNView!
    private var url: URL!
    
    @IBOutlet weak var closeButton: UIButton!
    
    convenience public init(url: URL) {
        self.init()
        self.url = url
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        self.navigationController?.isNavigationBarHidden = true
        
        self.sceneView = ARSCNView(frame: self.view.frame)
        self.view.addSubview(self.sceneView)
        
        // Add close button
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 50))
        button.setTitle("X Exit", for: .normal)
        button.addTarget(self, action: #selector(close), for: .touchUpInside)
        self.view.addSubview(button)
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        //        sceneView.showsStatistics = true
        
        let mainScene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = mainScene
        
        // Setup the AR Session
        //        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,ARSCNDebugOptions.showWorldOrigin]
        self.sceneView.debugOptions = []
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        configuration.isLightEstimationEnabled = true
        self.sceneView.session.run(configuration)
        
        loadModel()
    }
    
    func close(sender: UIButton!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadModel() {
        //        let scene = SCNScene(named: "art.scnassets/Raptor.scn")
        //        scene?.rootNode.position = SCNVector3(0, 0, -5)
        //        self.sceneView.scene.rootNode.addChildNode((scene?.rootNode)!)
        
        do {
            let scene = try SCNScene(url: self.url, options: [SCNSceneSource.LoadingOption.animationImportPolicy : SCNSceneSource.AnimationImportPolicy.play])
            scene.rootNode.position = SCNVector3(0, 0, -5)
            self.sceneView.scene.rootNode.addChildNode(scene.rootNode)
        } catch {
            print ("error loading scn file")
        }
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        let estimate = self.sceneView.session.currentFrame?.lightEstimate
        if(estimate == nil) {
            return
        }
        
        let intensity = (estimate?.ambientIntensity)! / 1000.0
        self.sceneView.scene.lightingEnvironment.intensity = intensity
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
}


