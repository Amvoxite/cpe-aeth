//
//  LandscapeNavigationController.swift
//

import UIKit

open class PortraitNavigationController: UINavigationController {
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
}

