//
//  MSScreenModel.h
//  AetherPlayer
//
//  Created by Stefan Vukanić on 3/15/18.
//

#import <Foundation/Foundation.h>
#import "MSScreenParameters.h"

//TODO: implement

@interface MSScreenModel : NSObject
@property(nonatomic, strong) MSScreenParameters *parameters;
@end
