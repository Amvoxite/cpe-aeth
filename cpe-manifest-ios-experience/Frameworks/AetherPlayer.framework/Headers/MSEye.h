//
//  MSEye.h
//  AetherPlayer
//
//  Created by Stefan Vukanić on 3/14/18.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MSEye) {
    left,
    right,
};
