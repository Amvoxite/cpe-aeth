//
//  UIViewExtensions.h
//  AetherPlayer
//
//  Created by Jovan Erčić on 2/9/18.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewExtensions)
- (void)fillView:(UIView *)view;
@end
