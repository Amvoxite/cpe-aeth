//
//  ATHUtil.h
//  Pods
//
//  Created by Miloš Žikić on 7/2/18.
//

#import <Foundation/Foundation.h>

@interface ATHUtil : NSObject

+(double)seekTime:(double) time withIntendedTime:(double) fallbackTime;
    
@end
