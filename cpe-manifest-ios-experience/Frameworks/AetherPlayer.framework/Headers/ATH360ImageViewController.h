//
//  ATH360ImageViewController.h
//  AetherPlayer
//
//  Created by Jovan Erčić on 2/20/18.
//

#import "ATH360ContentViewController.h"

#import "ATHContent.h"

@interface ATH360ImageViewController : ATH360ContentViewController
+ (ATH360ImageViewController *)imageViewControllerWithContent:(ATHContent *)content;
@end
