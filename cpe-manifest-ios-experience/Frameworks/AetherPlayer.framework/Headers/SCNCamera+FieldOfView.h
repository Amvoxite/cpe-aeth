//
//  SCNCamera+FieldOfView.h
//  Pods
//
//  Created by Miloš Žikić on 4/5/18.
//

#import <SceneKit/SceneKit.h>

@interface SCNCamera (FieldOfView)
@property CGFloat fov;
@end
