//
//  ATHPreviewView.h
//  AetherPlayer
//
//  Created by Jovan Erčić on 6/21/18.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "ATHAVPlayerView.h"

@interface ATHPreviewView : ATHAVPlayerView
@end
