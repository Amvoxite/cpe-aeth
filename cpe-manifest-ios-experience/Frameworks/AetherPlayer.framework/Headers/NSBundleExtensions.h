//
//  NSBundleExtensions.h
//  AetherPlayer
//
//  Created by Jovan Erčić on 2/16/18.
//

#import <Foundation/Foundation.h>

@interface NSBundle (NSBundleExtensions)
+ (NSBundle *)podBundle;
@end
