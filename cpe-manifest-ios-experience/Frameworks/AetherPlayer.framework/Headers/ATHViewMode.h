//
//  ATHViewMode.h
//  Pods
//
//  Created by Stefan Vukanić on 4/5/18.
//

#ifndef ATHViewMode_h
#define ATHViewMode_h

typedef NS_ENUM(NSInteger, ATHViewMode) {
    ATHViewModeRegular,
    ATHViewMode360,
    ATHViewModeCardboard,
};

#endif /* ATHViewMode_h */
